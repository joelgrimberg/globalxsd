import { useEffect, useState } from "react";
import Header from "~/components/header";
// Usage
function App() {
  // State and setters for ...
  // Search term
  const [searchTerm, setSearchTerm] = useState<string>("");
  // API search results
  const [results, setResults] = useState<any[]>([]);
  // Searching status (whether there is pending API request)
  const [isSearching, setIsSearching] = useState<boolean>(false);
  // Debounce search term so that it only gives us latest value ...
  // ... if searchTerm has not been updated within last 500ms.
  // The goal is to only have the API call fire when user stops typing ...
  // ... so that we aren't hitting our API rapidly.
  // We pass generic type, this case string
  const debouncedSearchTerm: string = useDebounce<string>(searchTerm, 500);
  // Effect for API call
  useEffect(
    () => {
      if (debouncedSearchTerm) {
        setIsSearching(true);
        searchCharacters(debouncedSearchTerm).then((results) => {
          setIsSearching(false);
          setResults(results);
          console.log(debouncedSearchTerm);
        });
      } else {
        setResults([]);
      }
    },
    [debouncedSearchTerm] // Only call effect if debounced search term changes
  );

  return (
    <div>
      <div className="flex min-h-full flex-col justify-center ">
        <div className="mx-auto w-full max-w-4xl px-8">
          <label
            htmlFor="alt"
            className="block text-sm font-medium text-gray-700"
          >
            Ask and thy shall receive
          </label>
          <div className="relative mt-1 rounded-md shadow-sm">
            <input
              className="block w-full rounded-md border-gray-300 pl-7 pr-12 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
              placeholder="🙏 please enter a deep question"
              onChange={(e: React.FormEvent<HTMLInputElement>) =>
                setSearchTerm((e.target as HTMLTextAreaElement).value)
              }
            />
          </div>

          {isSearching && <div>Searching ...</div>}

          {results.map(
            (result, index) =>
              index < 5 && (
                <div key={result.id}>
                  <h4>
                    <a href="/">{result.value}</a>
                  </h4>
                  <hr />
                </div>
              )
          )}
        </div>
      </div>
    </div>
  );
}
// API search function
function searchCharacters(search: string): Promise<any[]> {
  return fetch(`https://api.chucknorris.io/jokes/search?query=${search}`, {
    method: "GET",
  })
    .then((r) => r.json())
    .then((r) => r.result)
    .catch((error) => {
      console.error(error);
      return [];
    });
}
// Hook
// T is a generic type for value parameter, our case this will be string
function useDebounce<T>(value: T, delay: number): T {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState<T>(value);
  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );
  return debouncedValue;
}

export default function Apps() {
  return (
    <div>
      <Header page="Askchuck" />
      <App />
    </div>
  );
}
