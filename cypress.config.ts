import { defineConfig } from "cypress";

export default defineConfig({
  projectId: "1kns3r",
  e2e: {
    setupNodeEvents: (on, config) => {
      const isDev = config.watchForFileChanges;
      const port = process.env.PORT ?? (isDev ? "3000" : "8811");
      const configOverrides: Partial<Cypress.PluginConfigOptions> = {
        baseUrl: `http://localhost:${port}`,
        video: !process.env.CI,
        screenshotOnRunFailure: !process.env.CI,
        projectId: "ri2p5k",
      };

      return { ...config, ...configOverrides };
    },
  },
});
