# Cypress Paralellization Demo

## Topics

- [✅] 🗺️ Explore Repository
- [✅] 👷‍♂️ Create Gitlab Repository
- [ ] ➕ Add .gitlab-ci.yml
- [ ] 🚀 Run Test in CI
- [ ] 👷‍♂️ Create Cypress Dashboard
- [ ] ➕ Add Dashboard key to cypress configuration
- [ ] 📌 Push and watch Dashboard
- [ ] ➕ Add paralellization to gitlab yml and npm run **script**
- [ ] 🚀 push and watch
